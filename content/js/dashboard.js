/*
   Licensed to the Apache Software Foundation (ASF) under one or more
   contributor license agreements.  See the NOTICE file distributed with
   this work for additional information regarding copyright ownership.
   The ASF licenses this file to You under the Apache License, Version 2.0
   (the "License"); you may not use this file except in compliance with
   the License.  You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
var showControllersOnly = false;
var seriesFilter = "";
var filtersOnlySampleSeries = true;

/*
 * Add header in statistics table to group metrics by category
 * format
 *
 */
function summaryTableHeader(header) {
    var newRow = header.insertRow(-1);
    newRow.className = "tablesorter-no-sort";
    var cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Requests";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 3;
    cell.innerHTML = "Executions";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 7;
    cell.innerHTML = "Response Times (ms)";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 1;
    cell.innerHTML = "Throughput";
    newRow.appendChild(cell);

    cell = document.createElement('th');
    cell.setAttribute("data-sorter", false);
    cell.colSpan = 2;
    cell.innerHTML = "Network (KB/sec)";
    newRow.appendChild(cell);
}

/*
 * Populates the table identified by id parameter with the specified data and
 * format
 *
 */
function createTable(table, info, formatter, defaultSorts, seriesIndex, headerCreator) {
    var tableRef = table[0];

    // Create header and populate it with data.titles array
    var header = tableRef.createTHead();

    // Call callback is available
    if(headerCreator) {
        headerCreator(header);
    }

    var newRow = header.insertRow(-1);
    for (var index = 0; index < info.titles.length; index++) {
        var cell = document.createElement('th');
        cell.innerHTML = info.titles[index];
        newRow.appendChild(cell);
    }

    var tBody;

    // Create overall body if defined
    if(info.overall){
        tBody = document.createElement('tbody');
        tBody.className = "tablesorter-no-sort";
        tableRef.appendChild(tBody);
        var newRow = tBody.insertRow(-1);
        var data = info.overall.data;
        for(var index=0;index < data.length; index++){
            var cell = newRow.insertCell(-1);
            cell.innerHTML = formatter ? formatter(index, data[index]): data[index];
        }
    }

    // Create regular body
    tBody = document.createElement('tbody');
    tableRef.appendChild(tBody);

    var regexp;
    if(seriesFilter) {
        regexp = new RegExp(seriesFilter, 'i');
    }
    // Populate body with data.items array
    for(var index=0; index < info.items.length; index++){
        var item = info.items[index];
        if((!regexp || filtersOnlySampleSeries && !info.supportsControllersDiscrimination || regexp.test(item.data[seriesIndex]))
                &&
                (!showControllersOnly || !info.supportsControllersDiscrimination || item.isController)){
            if(item.data.length > 0) {
                var newRow = tBody.insertRow(-1);
                for(var col=0; col < item.data.length; col++){
                    var cell = newRow.insertCell(-1);
                    cell.innerHTML = formatter ? formatter(col, item.data[col]) : item.data[col];
                }
            }
        }
    }

    // Add support of columns sort
    table.tablesorter({sortList : defaultSorts});
}

$(document).ready(function() {

    // Customize table sorter default options
    $.extend( $.tablesorter.defaults, {
        theme: 'blue',
        cssInfoBlock: "tablesorter-no-sort",
        widthFixed: true,
        widgets: ['zebra']
    });

    var data = {"OkPercent": 100.0, "KoPercent": 0.0};
    var dataset = [
        {
            "label" : "FAIL",
            "data" : data.KoPercent,
            "color" : "#FF6347"
        },
        {
            "label" : "PASS",
            "data" : data.OkPercent,
            "color" : "#9ACD32"
        }];
    $.plot($("#flot-requests-summary"), dataset, {
        series : {
            pie : {
                show : true,
                radius : 1,
                label : {
                    show : true,
                    radius : 3 / 4,
                    formatter : function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">'
                            + label
                            + '<br/>'
                            + Math.round10(series.percent, -2)
                            + '%</div>';
                    },
                    background : {
                        opacity : 0.5,
                        color : '#000'
                    }
                }
            }
        },
        legend : {
            show : true
        }
    });

    // Creates APDEX table
    createTable($("#apdexTable"), {"supportsControllersDiscrimination": true, "overall": {"data": [0.0913978494623656, 500, 1500, "Total"], "isController": false}, "titles": ["Apdex", "T (Toleration threshold)", "F (Frustration threshold)", "Label"], "items": [{"data": [0.43548387096774194, 500, 1500, "getUpcomingEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeGroup"], "isController": false}, {"data": [0.02, 500, 1500, "findAllChildActiveSubsidies"], "isController": false}, {"data": [0.018518518518518517, 500, 1500, "getListDomainByLevel"], "isController": false}, {"data": [0.0, 500, 1500, "getPendingEvents"], "isController": false}, {"data": [0.16666666666666666, 500, 1500, "getPortfolioTermByYear"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCurrentFeeTier"], "isController": false}, {"data": [0.5, 500, 1500, "findAllClassResources"], "isController": false}, {"data": [0.0, 500, 1500, "listBulkInvoiceRequest"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCustomSubsidies"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedSubsidyFiles"], "isController": false}, {"data": [0.0, 500, 1500, "getAllChildDiscounts"], "isController": false}, {"data": [0.0, 500, 1500, "getChildFinancialAssistanceStatus"], "isController": false}, {"data": [0.08333333333333333, 500, 1500, "getMyDownloadPortfolio"], "isController": false}, {"data": [0.1951219512195122, 500, 1500, "getListDomain"], "isController": false}, {"data": [0.391304347826087, 500, 1500, "getLessonPlan"], "isController": false}, {"data": [0.0, 500, 1500, "getAdvancePaymentReceipts"], "isController": false}, {"data": [0.0, 500, 1500, "findAllProgramBillingUpload"], "isController": false}, {"data": [0.0, 500, 1500, "findAllChildHistorySubsidiesForBillingAdjustment"], "isController": false}, {"data": [0.03125, 500, 1500, "getChildSemesterEvaluation"], "isController": false}, {"data": [0.07142857142857142, 500, 1500, "getCentreManagementConfig"], "isController": false}, {"data": [0.07692307692307693, 500, 1500, "updateClassResourceOrder"], "isController": false}, {"data": [0.0, 500, 1500, "getChildPortfolio"], "isController": false}, {"data": [0.4318181818181818, 500, 1500, "getStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCentreForSchool"], "isController": false}, {"data": [0.013888888888888888, 500, 1500, "findAllAbsentForVoidingSubsidyByChild"], "isController": false}, {"data": [0.0, 500, 1500, "portfolioByID"], "isController": false}, {"data": [0.0, 500, 1500, "invoicesByFkChild"], "isController": false}, {"data": [0.45, 500, 1500, "getCountStaffCheckInOutRecordsByRole"], "isController": false}, {"data": [0.0, 500, 1500, "findAllFeeDraft"], "isController": false}, {"data": [0.07142857142857142, 500, 1500, "getPastEvents"], "isController": false}, {"data": [0.0, 500, 1500, "findAllConsolidatedRefund"], "isController": false}, {"data": [0.07407407407407407, 500, 1500, "getMyDownloadAlbum"], "isController": false}, {"data": [0.0, 500, 1500, "getRefundChildBalance"], "isController": false}, {"data": [0.0, 500, 1500, "findAllUploadedGiroFiles"], "isController": false}, {"data": [0.0, 500, 1500, "findAllInvoice"], "isController": false}, {"data": [0.8260869565217391, 500, 1500, "getAllArea"], "isController": false}, {"data": [0.0, 500, 1500, "getChildChecklist"], "isController": false}, {"data": [0.0, 500, 1500, "bankAccountInfoByIDChild"], "isController": false}, {"data": [0.0, 500, 1500, "findAllCreditDebitNotes"], "isController": false}]}, function(index, item){
        switch(index){
            case 0:
                item = item.toFixed(3);
                break;
            case 1:
            case 2:
                item = formatDuration(item);
                break;
        }
        return item;
    }, [[0, 0]], 3);

    // Create statistics table
    createTable($("#statisticsTable"), {"supportsControllersDiscrimination": true, "overall": {"data": ["Total", 1116, 0, 0.0, 47003.12634408597, 9, 372996, 28383.5, 109007.00000000001, 152403.84999999992, 355063.24999999994, 2.620591840472646, 127.28558571387059, 4.695307308275294], "isController": false}, "titles": ["Label", "#Samples", "FAIL", "Error %", "Average", "Min", "Max", "Median", "90th pct", "95th pct", "99th pct", "Transactions/s", "Received", "Sent"], "items": [{"data": ["getUpcomingEvents", 31, 0, 0.0, 3447.096774193548, 35, 38516, 995.0, 6220.200000000001, 26439.79999999997, 38516.0, 0.0945493807015564, 0.05512302761604412, 0.13573006799930462], "isController": false}, {"data": ["findAllFeeGroup", 28, 0, 0.0, 17125.28571428571, 2262, 59727, 11734.0, 41766.90000000001, 54669.899999999965, 59727.0, 0.08438233636607469, 0.07746034783604512, 0.09089230176931679], "isController": false}, {"data": ["findAllChildActiveSubsidies", 25, 0, 0.0, 28688.04, 1478, 85737, 26453.0, 61020.000000000044, 82244.99999999999, 85737.0, 0.07939557734875938, 0.08926419950203093, 0.1294054869092572], "isController": false}, {"data": ["getListDomainByLevel", 27, 0, 0.0, 28746.999999999996, 1341, 76734, 29491.0, 51512.2, 67791.19999999995, 76734.0, 0.08382099560715894, 0.44277058465144437, 0.149060579102184], "isController": false}, {"data": ["getPendingEvents", 25, 0, 0.0, 37565.759999999995, 2275, 94250, 29204.0, 72500.40000000004, 91078.7, 94250.0, 0.07033674419651524, 0.03509968387150321, 0.09149271803687334], "isController": false}, {"data": ["getPortfolioTermByYear", 51, 0, 0.0, 12460.823529411766, 42, 78066, 6416.0, 32103.200000000004, 41422.599999999984, 78066.0, 0.16185183892251104, 0.16675474963742015, 0.205355099165987], "isController": false}, {"data": ["findAllCurrentFeeTier", 28, 0, 0.0, 100403.85714285714, 57344, 156569, 97561.5, 153113.0, 155386.4, 156569.0, 0.07830634561064967, 0.6863193963769332, 0.19821293732695697], "isController": false}, {"data": ["findAllClassResources", 27, 0, 0.0, 2539.814814814815, 14, 11499, 855.0, 10608.4, 11256.999999999998, 11499.0, 0.09817789106617554, 0.057621984893331535, 0.13633687607041173], "isController": false}, {"data": ["listBulkInvoiceRequest", 19, 0, 0.0, 43015.73684210526, 5090, 84701, 38237.0, 82772.0, 84701.0, 84701.0, 0.053455023224800884, 0.0802374844910097, 0.08096556740397086], "isController": false}, {"data": ["findAllCustomSubsidies", 25, 0, 0.0, 30077.16, 4103, 97399, 23728.0, 69814.80000000005, 92522.49999999999, 97399.0, 0.070229453671034, 0.08416012420781176, 0.0877868170887925], "isController": false}, {"data": ["findAllUploadedSubsidyFiles", 24, 0, 0.0, 26961.750000000004, 6257, 76353, 26274.0, 41939.5, 68374.5, 76353.0, 0.0669288044842299, 5.598187628105915, 0.09640623692796788], "isController": false}, {"data": ["getAllChildDiscounts", 24, 0, 0.0, 25956.541666666668, 1872, 88577, 22230.5, 64320.5, 83329.75, 88577.0, 0.06747296864193783, 0.041248123408059646, 0.111159080174755], "isController": false}, {"data": ["getChildFinancialAssistanceStatus", 23, 0, 0.0, 58339.17391304348, 32456, 95792, 50983.0, 92062.20000000001, 95788.6, 95792.0, 0.06454853909817271, 0.04645729815952469, 0.08465692188363862], "isController": false}, {"data": ["getMyDownloadPortfolio", 30, 0, 0.0, 7969.633333333333, 426, 28413, 5607.5, 21114.40000000001, 27761.8, 28413.0, 0.09828718204096609, 0.05778211287955233, 0.10087873859868689], "isController": false}, {"data": ["getListDomain", 41, 0, 0.0, 19574.9512195122, 46, 60939, 9286.0, 51869.000000000015, 59232.7, 60939.0, 0.12544440976875396, 0.49576297642104034, 0.20746552477833052], "isController": false}, {"data": ["getLessonPlan", 23, 0, 0.0, 4434.347826086958, 9, 25272, 3179.0, 11562.400000000003, 22727.999999999964, 25272.0, 0.0813097228752532, 0.05296248550565809, 0.13038141109488843], "isController": false}, {"data": ["getAdvancePaymentReceipts", 21, 0, 0.0, 34082.142857142855, 1764, 108697, 31761.0, 69591.6, 105169.49999999994, 108697.0, 0.05931801233814657, 0.08920872949291574, 0.09459600991034506], "isController": false}, {"data": ["findAllProgramBillingUpload", 15, 0, 0.0, 30743.399999999998, 9551, 66060, 22999.0, 64144.200000000004, 66060.0, 66060.0, 0.04351698467912026, 0.06012200077315177, 0.06489300352052406], "isController": false}, {"data": ["findAllChildHistorySubsidiesForBillingAdjustment", 24, 0, 0.0, 45990.41666666666, 6029, 93318, 41270.0, 84273.0, 91129.0, 93318.0, 0.07381640015870526, 0.10642645291436376, 0.13271093036345352], "isController": false}, {"data": ["getChildSemesterEvaluation", 32, 0, 0.0, 20114.59375, 808, 90049, 13754.0, 54333.2, 76292.39999999995, 90049.0, 0.09823454110655072, 0.07569048137994973, 0.1506135054075045], "isController": false}, {"data": ["getCentreManagementConfig", 21, 0, 0.0, 13807.285714285714, 665, 42311, 8683.0, 38936.8, 42165.6, 42311.0, 0.06932134404183048, 0.08177752304934689, 0.11224100431772943], "isController": false}, {"data": ["updateClassResourceOrder", 26, 0, 0.0, 6977.423076923079, 372, 26129, 4148.5, 21888.100000000002, 25528.399999999998, 26129.0, 0.0854689616178617, 0.05041333282928561, 0.08129567247636454], "isController": false}, {"data": ["getChildPortfolio", 24, 0, 0.0, 151706.25000000003, 97175, 193326, 153113.5, 180611.0, 190385.75, 193326.0, 0.06344238374183178, 0.12858079800605876, 0.15346365676613022], "isController": false}, {"data": ["getStaffCheckInOutRecordsByRole", 22, 0, 0.0, 2975.681818181818, 53, 14147, 1129.5, 8077.9, 13263.949999999986, 14147.0, 0.07464155091571612, 0.04468288155403709, 0.13623540884909513], "isController": false}, {"data": ["findAllCentreForSchool", 45, 0, 0.0, 93771.95555555554, 15321, 185310, 88259.0, 132097.2, 148670.69999999992, 185310.0, 0.1250149323391405, 114.31788912026298, 0.30739077427998346], "isController": false}, {"data": ["findAllAbsentForVoidingSubsidyByChild", 36, 0, 0.0, 39958.86111111111, 966, 88138, 36441.5, 79192.90000000002, 87509.0, 88138.0, 0.10160507799600918, 0.06360239745648621, 0.13524191534039112], "isController": false}, {"data": ["portfolioByID", 23, 0, 0.0, 41481.08695652174, 6138, 86274, 33975.0, 80675.0, 85510.99999999999, 86274.0, 0.06586483390607102, 0.11030513942583048, 0.23149173557416955], "isController": false}, {"data": ["invoicesByFkChild", 31, 0, 0.0, 57075.935483870955, 8884, 133150, 51263.0, 103887.6, 128193.99999999999, 133150.0, 0.0868607677931481, 0.2546035112414646, 0.25243910639883665], "isController": false}, {"data": ["getCountStaffCheckInOutRecordsByRole", 20, 0, 0.0, 1809.7499999999995, 67, 6302, 812.0, 5122.700000000003, 6248.199999999999, 6302.0, 0.06909847223277893, 0.04170200765611072, 0.07550897502781213], "isController": false}, {"data": ["findAllFeeDraft", 29, 0, 0.0, 55471.24137931035, 36046, 107491, 46102.0, 94187.0, 103224.0, 107491.0, 0.07761170063293679, 0.044111337664423066, 0.12361785520734368], "isController": false}, {"data": ["getPastEvents", 21, 0, 0.0, 22010.952380952385, 295, 91532, 11034.0, 66880.00000000001, 89391.89999999997, 91532.0, 0.06282271430024472, 0.031165955922387023, 0.07914189594464421], "isController": false}, {"data": ["findAllConsolidatedRefund", 27, 0, 0.0, 82609.88888888888, 52839, 161732, 79698.0, 109819.2, 141456.7999999999, 161732.0, 0.07214949375105219, 0.10693511121710851, 0.12464106879454229], "isController": false}, {"data": ["getMyDownloadAlbum", 27, 0, 0.0, 8376.62962962963, 314, 32779, 4865.0, 23061.6, 29652.999999999985, 32779.0, 0.092980329494738, 0.0608367390248774, 0.1468253835869056], "isController": false}, {"data": ["getRefundChildBalance", 27, 0, 0.0, 27221.222222222226, 3409, 90938, 17184.0, 75141.79999999999, 88766.4, 90938.0, 0.0759399680489616, 0.07245444217171433, 0.11887868045164593], "isController": false}, {"data": ["findAllUploadedGiroFiles", 33, 0, 0.0, 35946.454545454544, 4009, 77791, 32278.0, 65022.0, 69782.29999999997, 77791.0, 0.09299022757244783, 26.568836940579246, 0.13303777675159775], "isController": false}, {"data": ["findAllInvoice", 59, 0, 0.0, 181453.0338983051, 5584, 372996, 145214.0, 357939.0, 359218.0, 372996.0, 0.13855391978432555, 0.29708963748775913, 0.4455507496295444], "isController": false}, {"data": ["getAllArea", 23, 0, 0.0, 788.8695652173915, 53, 4721, 168.0, 4166.200000000001, 4669.4, 4721.0, 0.08152414709791758, 0.0645145657953035, 0.07666772817899867], "isController": false}, {"data": ["getChildChecklist", 29, 0, 0.0, 80367.89655172414, 38372, 148499, 77448.0, 121227.0, 143581.0, 148499.0, 0.08558687510513904, 0.21100438780298492, 0.2818349051313759], "isController": false}, {"data": ["bankAccountInfoByIDChild", 20, 0, 0.0, 60200.0, 10463, 100261, 64858.5, 98091.90000000002, 100205.7, 100261.0, 0.05615832154008581, 0.08398795070562931, 0.12926285534177953], "isController": false}, {"data": ["findAllCreditDebitNotes", 30, 0, 0.0, 145641.90000000002, 94384, 234701, 139318.5, 187791.40000000002, 209730.99999999997, 234701.0, 0.0800837141758852, 0.162539699832892, 0.18707055108273182], "isController": false}]}, function(index, item){
        switch(index){
            // Errors pct
            case 3:
                item = item.toFixed(2) + '%';
                break;
            // Mean
            case 4:
            // Mean
            case 7:
            // Median
            case 8:
            // Percentile 1
            case 9:
            // Percentile 2
            case 10:
            // Percentile 3
            case 11:
            // Throughput
            case 12:
            // Kbytes/s
            case 13:
            // Sent Kbytes/s
                item = item.toFixed(2);
                break;
        }
        return item;
    }, [[0, 0]], 0, summaryTableHeader);

    // Create error table
    createTable($("#errorsTable"), {"supportsControllersDiscrimination": false, "titles": ["Type of error", "Number of errors", "% in errors", "% in all samples"], "items": []}, function(index, item){
        switch(index){
            case 2:
            case 3:
                item = item.toFixed(2) + '%';
                break;
        }
        return item;
    }, [[1, 1]]);

        // Create top5 errors by sampler
    createTable($("#top5ErrorsBySamplerTable"), {"supportsControllersDiscrimination": false, "overall": {"data": ["Total", 1116, 0, null, null, null, null, null, null, null, null, null, null], "isController": false}, "titles": ["Sample", "#Samples", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors", "Error", "#Errors"], "items": [{"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}, {"data": [], "isController": false}]}, function(index, item){
        return item;
    }, [[0, 0]], 0);

});
